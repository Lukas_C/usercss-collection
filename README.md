# A collection of (mostly dark) UserCSS files

## GNU-Dark

A dark userstyle for GNU.org, primarily developed for the emacs manual

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/gnu.org.user.css)


## Dark Go Documentation - Alternative

A dark userstyle for Go Package Documentation

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/alt-pkg.go.dev.user.css)


## Light Image Background for GitHub

Adds a light background for images on GitHub. Useful for seeing black text in transparent images on GitHub Dark.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/light_image-github.com.user.css)


## Dark Go Documentation

A dark userstyle for Go Package Documentation

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/pkg.go.dev.user.css)


## Dark and Centered Freedesktop Specification

A dark userstyle for Freedesktop Specifications

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/specifications.freedesktop.org.user.css)


## RFC Dark and Centered

A dark userstyle for RFCs

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/tools.ietf.org.user.css)


## Haskell Tutorial Dark

A dark userstyle for the Tutorial on haskell.org/tutorial

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/tutorial-haskell.org.user.css)


## More space-efficient Moodle

More space-efficent, well-proportioned Moodle page

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/Lukas_C/usercss-collection/raw/master/moodle.tum.de.user.css)


# `generate-md.py`
Provides a quick and easy way of generating this readme, extracting UserCSS name and description from the file. Assumes youre hosting on gitlab.com.

#!/usr/bin/env python3
import os, sys

# Some quick variables for use with other repos. Assumes gitlab.com url structure.
user = 'Lukas_C'
repo = 'usercss-collection'

def generate_entry(filename, user, repo):
    metadata_template = '@{variable} '

    # Initialize in case the variables are not set in the file
    name = filename
    desc = ''
    with open(filename, 'r') as f:
        for line in f.readlines():
            # If this string is reached, all of the UserStyle data has been read
            if '==/UserStyle==' in line:
                break

            metadata_name = metadata_template.format(variable='name')
            if line.startswith(metadata_name):
                name = line.strip(metadata_name)
                continue

            metadata_desc = metadata_template.format(variable='description')
            if line.startswith(metadata_desc):
                desc = line.strip(metadata_desc)
                continue

    url = 'https://gitlab.com/{user}/{repo}/raw/master/{filename}'.format(user=user, repo=repo, filename=filename)
    template = '## {name}\n{desc}\n[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)]({url})\n\n\n'
    return template.format(name=name, desc=desc, url=url)



# Get all files in the current dir
filenames = [f for f in os.listdir('./') if os.path.isfile(f) and f.endswith(".user.css")]

with open('README.md', 'w') as readmefile:
    readmefile.write('# A collection of (mostly dark) UserCSS files\n\n')
    for filename in filenames:
        readmefile.write(generate_entry(filename, user, repo))

    readmefile.write('# `generate-md.py`\nProvides a quick and easy way of generating this readme, extracting UserCSS name and description from the file. Assumes youre hosting on gitlab.com.\n')
